(ns confapp.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [confapp.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[confapp started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[confapp has shut down successfully]=-"))
   :middleware wrap-dev})
