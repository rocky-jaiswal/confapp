(ns confapp.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[confapp started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[confapp has shut down successfully]=-"))
   :middleware identity})
