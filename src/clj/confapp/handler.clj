(ns confapp.handler
  (:require [confapp.middleware :as middleware]
            [confapp.layout :refer [error-page]]
            [confapp.routes.home :refer [home-routes]]
            [confapp.routes.admin :refer [admin-routes]]
            [compojure.core :refer [routes wrap-routes]]
            [ring.util.http-response :as response]
            [compojure.route :as route]
            [confapp.env :refer [defaults]]
            [mount.core :as mount]))

(mount/defstate init-app
  :start ((or (:init defaults) identity))
  :stop  ((or (:stop defaults) identity)))

(mount/defstate app
  :start
  (middleware/wrap-base
    (routes
      (-> #'home-routes
        (wrap-routes middleware/wrap-csrf)
        (wrap-routes middleware/wrap-formats))
      (-> #'admin-routes
        (wrap-routes middleware/wrap-csrf)
        (wrap-routes middleware/wrap-restricted)
        (wrap-routes middleware/wrap-formats))
     (route/not-found
      (:body
       (error-page {:status 404
                    :title "page not found"}))))))

