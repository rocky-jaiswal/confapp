(ns confapp.routes.admin
  (:require [confapp.layout :as layout]
            [confapp.db.core :as db]
            [compojure.core :refer [defroutes GET]]
            [clojure.java.io :as io]))

(defn admin-page []
  (let [sessions (db/get-sessions)]
    (layout/render "admin.html" {:sessions sessions})))

(defroutes admin-routes
  (GET "/admin" [] (admin-page)))
