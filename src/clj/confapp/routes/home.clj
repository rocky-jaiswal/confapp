(ns confapp.routes.home
  (:require [confapp.layout :as layout]
            [confapp.db.core :as db]
            [compojure.core :refer [defroutes GET POST]]
            [ring.util.http-response :as response]
            [confapp.validations.session :as v]
            [confapp.utils.core :as utils]
            [clojure.java.io :as io]))

;; GET page handlers

(defn home-page [{:keys [flash]}]
  (layout/render
    "home.html" (select-keys flash [:message :errors])))

(defn login-page [{:keys [flash]}]
  (layout/render
   "login.html" (select-keys flash [:message :errors])))

;; POST Handlers

(defn save-session! [{:keys [params]}]
  (if-let [errors (v/validate-session params)]
    (-> (response/found "/")
        (assoc :flash (assoc params :errors errors)))
    (do
      (db/save-session!
        (select-keys
          params
          [:presenter-name :session-title :session-duration :session-description]))
      (-> (response/found "/")
          (assoc :flash (assoc {} :message "Session saved successfully! Thanks for your submission."))))))

(defn login! [{:keys [params session]}]
  (if (utils/check-user (select-keys params [:username :password]))
    (-> (response/found "/admin")
        (assoc :session (assoc session :identity (params :username))))
    (-> (response/found "/login")
        (assoc :flash (assoc {} :errors {:error "Login error!"})))))

;; Routes

(defroutes home-routes
  (GET "/" request (home-page request))
  (GET "/login" request (login-page request))
  (POST "/login" request (login! request))
  (POST "/sessions" request (save-session! request)))

