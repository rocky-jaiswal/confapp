(ns confapp.utils.core
  (:require [confapp.db.core :as db]
            [buddy.hashers :as hashers]))

(defn to-secret [str]
  (hashers/derive str))

(defn safe-compare [password from-db]
  (hashers/check password from-db))

(defn check-user [{:keys [username password]}]
  (let [user-from-db (db/get-user-by-name {:username username})]
    (when user-from-db
      (safe-compare password (user-from-db :password)))))

(defn create-user [username password]
  (db/create-user! {:username username :password (to-secret password)}))
