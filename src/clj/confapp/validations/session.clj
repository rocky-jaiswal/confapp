(ns confapp.validations.session
  (:require [struct.core :as st]))

(def +scheme+
  [
    [:presenter-name st/required st/string]
    [:session-title st/required st/string]
    [:session-duration st/required st/integer-str]
    [:session-description st/required st/string [st/max-count 800]]])

(defn validate-session [params]
  (first (st/validate params +scheme+)))
