FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/confapp.jar /confapp/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/confapp/app.jar"]
