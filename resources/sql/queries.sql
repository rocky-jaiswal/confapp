-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(username, password)
VALUES (:username, :password)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users
SET username = :username, password = :password
WHERE id = :id

-- :name get-user :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE id = :id

-- :name get-user-by-name :? :1
-- :doc retrieves a user record given the username
SELECT * FROM users
WHERE username = :username LIMIT 1

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE id = :id

-- :name save-session! :! :n
-- :doc creates a new session
INSERT INTO sessions
(presenter_name, session_title, session_duration, session_description)
VALUES (:presenter-name, :session-title, :session-duration, :session-description)

-- :name get-sessions :? :*
-- :doc selects all available sessions
SELECT * from sessions
