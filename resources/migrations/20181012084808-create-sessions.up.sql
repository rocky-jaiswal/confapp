CREATE TABLE sessions
(id INTEGER PRIMARY KEY,
 presenter_name TEXT NOT NULL,
 session_title TEXT NOT NULL,
 session_duration INTEGER NOT NULL,
 session_description TEXT);
